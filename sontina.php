<?php
error_reporting(0);
include_once ('src/crest-master/src/crest.php');
$total = CRest::call('crm.deal.list', array('select'=>array('ID')));
$iteration = intval($total['total']/50) + 1;

######################################################################
###############_получаем_все_сделки_##################################
for ($i = 0; $i < $iteration; $i++) {
	$start = $i * 50;
	$dataDeals[] = array('method' => 'crm.deal.list',
		'params' => array(
			//'filter' => array('>=OPPORTUNITY' => '1'),
			'select' => array('ID', 'CONTACT_ID', 'CLOSEDATE'),
			'start' => $start
		)
	);
	
}

if (count($dataDeals) > 50) {
	$dataDeals = array_chunk($dataDeals, 50);
}
foreach ($dataDeals as $data) {
	$deals[] = CRest::callBatch($data);
}

###########################################################################
#####################_формируем_массив_idконтакт=>сделка_##################
for ($i = 0; $i < count($deals); $i++) {
	foreach ($deals[$i]['result']['result'] as $deal) {
		foreach ($deal as $d) {
			if (empty($d['CONTACT_ID'])) {
				continue;
			}
			$newArr[$d['CONTACT_ID']][] = array('id' => $d['ID'], 'closedate' => $d['CLOSEDATE']);
		}
	}
}
foreach ($newArr as $id => $value) {
	if (count($value) < 2) {
		unset($newArr[$id]);	
	}
}

######################################################################################
####_работа_с_датой_обновление_полей_повторная_сделка&средний_срок_между_сделками#####
// Средний срок между сделками
foreach ($newArr as $id => $value) {
	$first[$id] = explode('T',array_shift($value)['closedate']);
	$last[$id] = explode('T',array_pop ($value)['closedate']);
	$diff[$id] = round(intervalDays($first[$id][0], $last[$id][0]), PHP_ROUND_HALF_UP);
}
foreach ($diff as $k => $d) {
	if ($d < 0) {
		$d *= -1;
		$diff[$k] = $d;
	}
}
foreach ($newArr as $key => $value) {
	$diff[$key] /= count($value);
	$diff[$key] = round($diff[$key]);
}

#######################################################################################
#######################_макс_и_мин_срок_между_сделками_################################
foreach ($newArr as $key => $value) {
	for ($i = 0; $i < count($value); $i++) {
		$arrQ[$key][] = explode('T', $value[$i]['closedate'])[0];
	}
}
foreach ($arrQ as $k => $v) {
	for ($i = 0; $i < count($v); $i++) {
		if (round(intervalDays($v[$i+1], $v[$i])) == 0) continue;
		if (round(intervalDays($v[$i+1], $v[$i])) < 0) {
			$num[$k][] = round(intervalDays($v[$i+1], $v[$i])) * -1;
			continue;
		}
		$num[$k][] = round(intervalDays($v[$i+1], $v[$i]));
	}
}

foreach ($num as $k => $v) {
	$maxMin[$k]['min'] = min($v);
	$maxMin[$k]['max'] = max($v);
}

#########################################################################################
########################_последняя_дата_сделки_##########################################
foreach ($newArr as $key => $value) {
	$date = explode('T',array_pop($value)['closedate']);
	$lastDate[$key] = $date[0];
}

#########################################################################################
#########################_update_########################################################
foreach ($diff as $k => $data) {
	$dataUpdate[] = array('method' => 'crm.contact.update',
		'params' => array(
			'id' => $k,
			'fields' => array(
				'UF_CRM_RETURN' => 'Y',
				'UF_CRM_DIFF' => $data,
				'UF_CRM_MIN' => $maxMin[$k]['min'],
				'UF_CRM_MAX' => $maxMin[$k]['max'],
				'UF_CRM_CLOSE' => $lastDate[$k],
			)
		),
	);
}
if (count($dataUpdate) > 50) {
	$dataUpdate = array_chunk($dataUpdate, 50);
}
foreach ($dataUpdate as $data) {
	//$update[] = CRest::callBatch($data);
}

echo '<pre>';
print_r ($dataUpdate);
echo '</pre>';

####################################################################################
#################################  functions  ######################################
####################################################################################
function intervalDays($CheckIn, $CheckOut){
$CheckInX = explode("-", $CheckIn);
$CheckOutX =  explode("-", $CheckOut);
$date1 =  mktime(0, 0, 0, $CheckInX[1],$CheckInX[2],$CheckInX[0]);
$date2 =  mktime(0, 0, 0, $CheckOutX[1],$CheckOutX[2],$CheckOutX[0]);
$interval =($date2 - $date1)/(3600*24);

// returns numberofdays
return $interval;
}